#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import setting
import pickle
from underthesea import word_sent
import numpy as np
def transform(X, y=None):
    result = X.apply(lambda text: word_sent(text,format='text'))
    # result = X.apply(lambda text: ViTokenizer.tokenize(text))
    return result
def load_pkl(file_name): #load moi truong da duoc training ra 
    with open(file_name, 'rb') as handle:
        result = pickle.load(handle)
    return result

def predict(data_input,enviroment): # nap moi truong    
    test1 = transform(data_input['feature'])
    X_test = enviroment[1].transform(enviroment[2].transform(test1))
    predicted = enviroment[0].predict(X_test)
    proba = enviroment[0].predict_proba(X_test)
    classes = enviroment[0].classes_
    return predicted[0],test1,proba,classes
def data_input(): # get du lieu nhap vao
    print ('Vui long nhap cau ban muon du doan : \n') 
    test = raw_input()
    test_data = []
    test_data.append({"feature":unicode(str(test),"utf-8"), "target":None})
    df_test = pd.DataFrame(test_data)
    return df_test
def estimate(classes,proba,estimate):
    # X1 = []
    # Y1 = []
    # for x in range(len(classes)):
    #     X1.append(classes[x].encode('utf-8'))
    # for y in range(len(proba)):
    #     Y1.append(proba[y])
    # print([k for  k in zip(X1, Y1[0])])

    index = np.where( classes == estimate )[0][0]
    # print (index)
    print (proba[0][index])
if __name__ == '__main__':
   
    domain = load_pkl(setting.ENVIRONMENT_DOMAIN)
    attribute = load_pkl(setting.ENVIRONMENT_ATTRIBUTE)
    question = load_pkl(setting.ENVIRONMENT_QUESTION)
    a = 1
    while a == 1:
        print ("\t\t1.Nhập vào một câu : \n")
        print ("\t\t2.Dừng lại\n")

        print ("Vui lòng chọn : ")
        choice = input()
        if choice !=1  :
            a=0
        if choice == 1 : 
            df_test = data_input()
            print ("=========Kết quả dự đoán : ==============\n")
            du_doan,clear_text,proba,classes = predict(df_test,domain)
            print ("Clear_text : " + clear_text[0])
            print ("Dự đoán domain    : " + str(du_doan))
            estimate(classes,proba,du_doan)

            du_doan,clear_text,proba,classes = predict(df_test,attribute)
            print ("Dự đoán attribute  : " + str(du_doan))
            estimate(classes,proba,du_doan)

            du_doan,clear_text,proba,classes = predict(df_test,question)
            print ("Dự đoán question : " + str(du_doan) )
            estimate(classes,proba,du_doan)

            print ("\n\n")
