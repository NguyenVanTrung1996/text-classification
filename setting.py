import os

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
DATA_TRAIN_PATH_ATTRIBUTE = os.path.join(DIR_PATH, './data_train/train/question_attribute/*.txt')
DATA_TRAIN_PATH_QUESTION = os.path.join(DIR_PATH, './data_train/train/question_type/*.txt')
DATA_TRAIN_PATH_DOMAIN = os.path.join(DIR_PATH, './data_train/train/domain/*.txt')

TRAIN_ATTRIBUTE = os.path.join(DIR_PATH, './data_train/attribute.json')
TRAIN_QUESTION = os.path.join(DIR_PATH, './data_train/question.json')
TRAIN_DOMAIN = os.path.join(DIR_PATH, './data_train/domain.json')

DATA_TRAIN_ATTRIBUTE = os.path.join(DIR_PATH, './data_train/train_attribute.json')
DATA_TRAIN_QUESTION = os.path.join(DIR_PATH, './data_train/train_question.json')
DATA_TRAIN_DOMAIN = os.path.join(DIR_PATH, './data_train/train_domain.json')

ENVIRONMENT_ATTRIBUTE = os.path.join(DIR_PATH, './environment/attribute.pkl')
ENVIRONMENT_QUESTION = os.path.join(DIR_PATH, './environment/question.pkl')
ENVIRONMENT_DOMAIN = os.path.join(DIR_PATH, './environment/domain.pkl')

